import ru.tinkoff.piapi.core.InvestApi

suspend fun old() {
    val config = readConfig()
    val api = TinkoffApi(config.token)
//    println(api.getUserInfo())
//    print(api.getAccounts())
    val acc = api.getAccountByName("Брокерский счёт")
//    println(api.getPortfolio(acc.id).positionsList)
    val positions = api.getPositions(acc.id)
    println("free money:")
    for (position in positions.moneyList.listIterator()) {
        println(doublePrice(position))
    }
    val usdrubExchange = api.getUsdRubExchangeCourse()
    var positions_cost = 0.0;
    for (position in positions.securitiesList.listIterator()) {
        val instrument = api.getInstrumentByFigi(position.figi)
        println(instrument.name)
        println(instrument.currency)
        println(position.balance + position.blocked)
        val price = api.getLastPrice(position.figi)
        println("price: $price")
        var costInCurrency = (position.balance + position.blocked) * price
        if (position.instrumentType == "bond") {
            costInCurrency /= 100 // цена в процентах
            val bond = api.getBondByFigi(position.figi)
            costInCurrency *= bond.nominal.let {
                doublePrice(it).apply { println(this) }
            }
            costInCurrency += (position.balance + position.blocked) * bond.aciValue.let { doublePrice(it).apply { println("нкд: $this") } }

        }
        println("price native: $costInCurrency")
        val rubPrice = when (instrument.currency) {
            "rub" -> costInCurrency
            "usd" -> costInCurrency * usdrubExchange
            else -> {
                throw Exception("Unsupported currency ${instrument.currency}")
            }
        }
        println("price rub: $rubPrice")
        positions_cost += rubPrice
    }
    println("Overall positions cost: ${positions_cost.toLong()}")
}

fun main(args: Array<String>) {
    val config = readConfig()
    val channel = Channel().slowdownChannel(config.token, null, "invest-public-api.tinkoff.ru:443")
    val api = InvestApi.createReadonly(channel)
    val adviser = Adviser(api, "Брокерский счёт")
//    println(adviser.overallCostOfSecurityPositions("usd"))
//    println(adviser.overallCostOfSecurityPositions("rub"))
    adviser.advise()
}