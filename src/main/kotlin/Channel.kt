import io.grpc.*
import io.grpc.Channel
import io.grpc.ForwardingClientCall.SimpleForwardingClientCall
import io.grpc.ForwardingClientCallListener.SimpleForwardingClientCallListener
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder
import io.grpc.netty.shaded.io.netty.channel.ChannelOption
import io.grpc.stub.MetadataUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import ru.tinkoff.piapi.core.InvestApi
import java.lang.Thread.sleep
import java.time.Duration
import java.util.concurrent.TimeUnit
import javax.annotation.Nonnull

class Channel {

    internal class LoggingClientCallListener<RespT>(
        listener: ClientCall.Listener<RespT>?,
        private val logger: Logger,
        private val method: MethodDescriptor<*, RespT>
    ) : SimpleForwardingClientCallListener<RespT>(listener) {
        @Volatile
        private var lastTrackingId: String? = null
        override fun onHeaders(headers: Metadata) {
            lastTrackingId = headers.get(trackingIdKey)
            delegate().onHeaders(headers)
        }

        override fun onMessage(message: RespT) {
            if (method.type == MethodDescriptor.MethodType.UNARY) {
                logger.debug(
                    "Пришёл ответ от метода {} сервиса {}. (x-tracking-id = {})",
                    method.bareMethodName,
                    method.serviceName,
                    lastTrackingId
                )
            } else {
                logger.debug(
                    "Пришло сообщение от потока {} сервиса {}.",
                    method.bareMethodName,
                    method.serviceName
                )
            }
            delegate().onMessage(message)
        }

        companion object {
            private val trackingIdKey = Metadata.Key.of("x-tracking-id", Metadata.ASCII_STRING_MARSHALLER)
        }
    }

    internal class LoggingClientCall<ReqT, RespT>(
        call: ClientCall<ReqT, RespT>?,
        private val logger: Logger,
        private val method: MethodDescriptor<ReqT, RespT>
    ) : SimpleForwardingClientCall<ReqT, RespT>(call) {
        override fun start(responseListener: Listener<RespT>, headers: Metadata) {
            logger.debug(
                "Готовится вызов метода {} сервиса {}.",
                method.bareMethodName,
                method.serviceName
            )
            super.start(
                LoggingClientCallListener(responseListener, logger, method),
                headers
            )
        }
    }

    internal class LoggingInterceptor : ClientInterceptor {
        private val logger = LoggerFactory.getLogger(LoggingInterceptor::class.java)
        override fun <ReqT, RespT> interceptCall(
            method: MethodDescriptor<ReqT, RespT>, callOptions: CallOptions, next: Channel
        ): ClientCall<ReqT, RespT> {
            return LoggingClientCall(
                next.newCall(method, callOptions), logger, method
            )
        }
    }

    internal class TimeoutInterceptor(private val timeout: Duration) : ClientInterceptor {
        override fun <ReqT, RespT> interceptCall(
            method: MethodDescriptor<ReqT, RespT>, callOptions: CallOptions, next: Channel
        ): ClientCall<ReqT, RespT> {
            var callOptions = callOptions
            if (method.type == MethodDescriptor.MethodType.UNARY) {
                callOptions = callOptions.withDeadlineAfter(timeout.toMillis(), TimeUnit.MILLISECONDS)
            }
            return next.newCall(method, callOptions)
        }
    }

    internal class SlowdownInterceptor() : ClientInterceptor {
        override fun <ReqT, RespT> interceptCall(
            method: MethodDescriptor<ReqT, RespT>, callOptions: CallOptions, next: Channel
        ): ClientCall<ReqT, RespT> {
            val delay = 70L
//            val delay = 0L
            sleep(delay) // 1000 запросов в минуту: https://tinkoff.github.io/investAPI/limits/
            return next.newCall(method, callOptions)
        }
    }

    @Nonnull
    fun slowdownChannel(token: String, appName: String?, target: String): Channel {
        val headers = Metadata()
        InvestApi.addAuthHeader(headers, token!!)
        InvestApi.addAppNameHeader(headers, appName)
        val connectionTimeout = Duration.parse("PT1S")
        val requestTimeout = Duration.parse("PT60S")
        return NettyChannelBuilder
            .forTarget(target)
            .intercept(
                LoggingInterceptor(),
                MetadataUtils.newAttachHeadersInterceptor(headers),
                TimeoutInterceptor(requestTimeout),
                SlowdownInterceptor(),
            )
            .withOption(
                ChannelOption.CONNECT_TIMEOUT_MILLIS, connectionTimeout.toMillis().toInt()
            ) // Намерено сужаем тип - предполагается,
            // что таймаут имеет разумную величину.
            .useTransportSecurity()
            .keepAliveTimeout(60, TimeUnit.SECONDS)
            .build()
    }
}