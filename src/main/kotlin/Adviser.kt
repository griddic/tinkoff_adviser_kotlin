import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory
import com.fasterxml.jackson.module.kotlin.KotlinFeature
import com.fasterxml.jackson.module.kotlin.KotlinModule
import kotlinx.coroutines.DelicateCoroutinesApi
import ru.tinkoff.piapi.contract.v1.Account
import ru.tinkoff.piapi.core.InvestApi
import ru.tinkoff.piapi.core.models.SecurityPosition
import java.nio.file.Files
import java.nio.file.Path

data class Config(
    val token: String
)

fun readConfig(): Config {
    val mapper = ObjectMapper(YAMLFactory())
    mapper.registerModule(
        KotlinModule.Builder()
            .withReflectionCacheSize(512)
            .configure(KotlinFeature.NullToEmptyCollection, false)
            .configure(KotlinFeature.NullToEmptyMap, false)
            .configure(KotlinFeature.NullIsSameAsDefault, false)
            .configure(KotlinFeature.SingletonSupport, false)
            .configure(KotlinFeature.StrictNullChecks, false)
            .build()
    )
    val config = Files.newBufferedReader(Path.of(".secrets/secrets.yaml")).use {
        mapper.readValue(it, Config::class.java)
    }
    return config
}


@OptIn(DelicateCoroutinesApi::class)
class Adviser(api: InvestApi, accountName: String) {
    private var api: InvestApi

    init {
        this.api = api
    }

    private val accountId: String by lazy {
        this.getAccountByName(accountName).id
    }
    private val positions by lazy {
        this.api.operationsService.getPositions(accountId).get()
    }
    private val positionByFigi: Map<String, SecurityPosition> by lazy {
        positions.securities.associateBy { it.figi }
    }
    private val positionPriceByFigi by lazy {
        (api.marketDataService
            .getLastPrices(positions.securities.map { it.figi }).get()
            .associateBy(
                keySelector = { it.figi },
                valueTransform = { doublePrice(it.price) }
            )
                )
    }
    private val bondByFigi by lazy {
        api.instrumentsService.allBonds.get().associateBy { it.figi }
    }
    private val etfByFigi by lazy {
        api.instrumentsService.allEtfs.get().associateBy { it.figi }
    }
    private val shareByFigi by lazy {
        api.instrumentsService.allShares.get().associateBy { it.figi }
    }
    private val usdExchange: Double by lazy {
        api.marketDataService.getLastPrices(listOf("FUTUSDRUBF00")).get()
            .also { assert(it.size == 1) }
            .first().price.let { doublePrice(it) }
    }

    private fun definePositionInstrumentType(figi: String): String {
        if (figi in shareByFigi.keys) {
            return "share"
        }
        if (figi in bondByFigi.keys) {
            return "bond"
        }
        if (figi in etfByFigi.keys) {
            return "etf"
        }
        throw Exception("Figi $figi in not etf nor bond nor share.")
    }

    private fun definePositionCurrency(figi: String): String {
        return when (definePositionInstrumentType(figi)) {
            "share" -> {
                shareByFigi.getValue(figi).currency
            }
            "bond" -> {
                bondByFigi.getValue(figi).currency
            }
            "etf" -> {
                etfByFigi.getValue(figi).currency
            }
            else -> throw Exception("unknown instrument type ${definePositionInstrumentType(figi)}")
        }
    }
    private fun definePositionCost(figi: String): Double {
        val position = positionByFigi.getValue(figi)
        return when (definePositionInstrumentType(position.figi)) {
            "share" -> {
                positionPriceByFigi.getValue(position.figi) * (position.blocked + position.balance)
            }
            "bond" -> {
                val bond = bondByFigi.getValue(position.figi)
                val relativePrice = positionPriceByFigi.getValue(position.figi) / 100
                ((position.blocked + position.balance)
                        * (relativePrice * doublePrice(bond.nominal) + doublePrice(bond.aciValue)))
            }
            "etf" -> {
                positionPriceByFigi.getValue(position.figi) * (position.blocked + position.balance)
            }
            else -> throw Exception("unknown instrument type ${definePositionInstrumentType(position.figi)}")
        }
    }

    private fun getAccountByName(name: String): Account {
        val accounts = api.userService.accounts.get()
        for (acc in accounts.listIterator()) {
            if (acc.name == name) {
                return acc
            }
        }
        throw Exception("No account with name: $name")
    }

    private fun overallCostOfSecurityPositions(currency: String, types: Collection<String>? = null): Double {
        var figis = positionByFigi.keys
            .filter { definePositionCurrency(it) == currency }
        types?.run {
            figis = figis.filter { definePositionInstrumentType(it) in types}
        }
        return figis.sumOf { definePositionCost(it) }
    }


    fun advise(): Unit {
        val usdPosCost = overallCostOfSecurityPositions("usd")
        val usdPosCostInRub = usdPosCost * usdExchange
        val rubPosCost = overallCostOfSecurityPositions("rub")
        println("usd: ${usdPosCostInRub / (usdPosCostInRub + rubPosCost)}")
        println("rub: ${rubPosCost / (usdPosCostInRub + rubPosCost)}")
        println("usd distribution:")
        val usdBondsCost = overallCostOfSecurityPositions("usd", listOf("bond"))
        val usdSharesCost = overallCostOfSecurityPositions("usd", listOf("share"))
        println("bonds: ${usdBondsCost / usdPosCost}")
        println("shares: ${usdSharesCost / usdPosCost}")
        println("rub distribution:")
        val rubBondsCost = overallCostOfSecurityPositions("rub", listOf("bond"))
        val rubSharesCost = overallCostOfSecurityPositions("rub", listOf("share"))
        println("bonds: ${rubBondsCost / rubPosCost}")
        println("shares: ${rubSharesCost / rubPosCost}")

    }
}