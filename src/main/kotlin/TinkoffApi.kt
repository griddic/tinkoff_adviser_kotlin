import io.grpc.InternalClientInterceptors
import io.grpc.ManagedChannelBuilder
import io.grpc.Metadata
import io.grpc.stub.MetadataUtils
import kotlinx.coroutines.*
import ru.tinkoff.piapi.contract.v1.*

fun doublePrice(units: Long, nano: Int): Double {
    return units.toDouble() + nano.toDouble() / 1000000000
}

fun doublePrice(quotation: Quotation): Double {
    return doublePrice(quotation.units, quotation.nano)
}

fun doublePrice(money: MoneyValue): Double {
    return doublePrice(money.units, money.nano)
}

class TinkoffApi(token: String) {
    private var instrumentsStub: InstrumentsServiceGrpcKt.InstrumentsServiceCoroutineStub
    private var marketdataStub: MarketDataServiceGrpcKt.MarketDataServiceCoroutineStub
    private var ordersStub: OperationsServiceGrpcKt.OperationsServiceCoroutineStub
    private var userStub: UsersServiceGrpcKt.UsersServiceCoroutineStub

    init {
        val channel = ManagedChannelBuilder.forAddress("invest-public-api.tinkoff.ru", 443)
            .useTransportSecurity()
            .build()
        val authHeader = Metadata()
        val key: Metadata.Key<String> = Metadata.Key.of("Authorization", Metadata.ASCII_STRING_MARSHALLER)
        authHeader.put(key, "Bearer $token")
        val authInterceptor = MetadataUtils.newAttachHeadersInterceptor(authHeader)
        this.userStub = UsersServiceGrpcKt.UsersServiceCoroutineStub(channel)
            .withInterceptors(authInterceptor)
        this.ordersStub = OperationsServiceGrpcKt.OperationsServiceCoroutineStub(channel)
            .withInterceptors(authInterceptor)
        this.marketdataStub = MarketDataServiceGrpcKt.MarketDataServiceCoroutineStub(channel)
            .withInterceptors(authInterceptor)
        this.instrumentsStub = InstrumentsServiceGrpcKt.InstrumentsServiceCoroutineStub(channel)
            .withInterceptors(authInterceptor)
    }

    suspend fun getUserInfo(): GetInfoResponse {
        val request = GetInfoRequest.newBuilder().build()

        val resultDeferred: Deferred<GetInfoResponse> = GlobalScope.async {
            userStub.getInfo(request)
        }
        return resultDeferred.await()
    }

    suspend fun getAccounts(): GetAccountsResponse {
        val request = GetAccountsRequest.newBuilder().build()

        val resultDeferred: Deferred<GetAccountsResponse> = GlobalScope.async {
            userStub.getAccounts(request)
        }
        return resultDeferred.await()
    }

    suspend fun getAccountByName(name: String): Account {
        for (acc in getAccounts().accountsList.listIterator()) {
            if (acc.name == name) {
                return acc
            }
        }
        throw Exception("No account with name: $name")
    }

    suspend fun getPortfolio(accountId: String): PortfolioResponse {
        return withContext(Dispatchers.Default) {
            ordersStub.getPortfolio(
                PortfolioRequest.newBuilder()
                    .setAccountId(accountId)
                    .setCurrency(PortfolioRequest.CurrencyRequest.RUB)
                    .build()
            )
        }
    }
    suspend fun getPositions(accountId: String): PositionsResponse {
        return withContext(Dispatchers.Default) {
            ordersStub.getPositions(
                PositionsRequest.newBuilder()
                    .setAccountId(accountId)
                    .build()
            )
        }
    }
    suspend fun getLastPrice(instrumentId: String): Double {
        val resp = withContext(Dispatchers.Default) {
            marketdataStub.getLastPrices(
                GetLastPricesRequest.newBuilder()
                    .addInstrumentId(instrumentId)
                    .build()
            )
        }
        assert(resp.lastPricesList.size == 1)
        val lastPrice = resp.getLastPrices(0)
        return doublePrice(lastPrice.price)
    }
    suspend fun getUsdRubExchangeCourse(): Double {
        return getLastPrice("FUTUSDRUBF00")
    }
    suspend fun getClosingPrice(instrumentId: String): Double {
        val resp = withContext(Dispatchers.Default) {
            marketdataStub.getClosePrices(
                GetClosePricesRequest.newBuilder()
                    .setInstruments(0, InstrumentClosePriceRequest.newBuilder()
                        .setInstrumentId(instrumentId)
                        .build())
                    .build()
            )
        }
        return doublePrice(resp.getClosePrices(0).price)
    }
    suspend fun getInstrumentByFigi(figi: String): Instrument {
        val resp = withContext(Dispatchers.Default) {
            instrumentsStub.getInstrumentBy(InstrumentRequest.newBuilder()
                .setIdType(InstrumentIdType.INSTRUMENT_ID_TYPE_FIGI)
                .setId(figi)
                .build())
        }
        return resp.instrument
    }
    suspend fun getBondByFigi(figi: String): Bond {
        val resp = withContext(Dispatchers.Default) {
            instrumentsStub.bondBy(InstrumentRequest.newBuilder()
                .setIdType(InstrumentIdType.INSTRUMENT_ID_TYPE_FIGI)
                .setId(figi)
                .build())
        }
        return resp.instrument
    }
}